FROM python:3-alpine

EXPOSE 9001

RUN mkdir app
COPY requirements.txt .
RUN apk update \
  && apk add -t build-deps gcc musl-dev \
  && pip install -r requirements.txt \
  && apk del build-deps
COPY . .
ENTRYPOINT ["python", "websocket_server.py"]
