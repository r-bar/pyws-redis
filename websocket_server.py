from argparse import ArgumentParser

import asyncio
import websockets
import aioredis
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


def arg_parser():
    parser = ArgumentParser()
    parser.add_argument('--port', '-p', default=9001, type=int)
    parser.add_argument('--host', '-H', default='0.0.0.0')
    parser.add_argument('--redis-url', '-r', default='redis://localhost')
    return parser


class App:
    def __init__(self, redis_url):
        self.redis_url = redis_url
        self.redis_connections = set()
        self.ws_connectections = set()
        self.channels = {}
        self.redis_pool = None

    @staticmethod
    def channel_name(path):
        if path == "/":
            return "default"
        return path[1:]

    async def server(self, websocket, path):
        """Returns a server instance for a given websocket connection"""
        logger.info(f"New connection {fmt_ws(websocket)}")
        channel_name = self.channel_name(path)
        if channel_name in self.channels.keys():
            channel = self.channels[channel_name]
        else:
            channel_conn = await aioredis.create_redis(self.redis_url)
            channel = (await channel_conn.subscribe(channel_name))[0]
        if not self.redis_pool:
            await self.init_redis_pool()

        handlers = [
            self.redis_handler(channel, websocket),
            self.websocket_handler(websocket, channel_name)
        ]

        # await asyncio.wait([asyncio.ensure_future(h) for h in handlers])
        await asyncio.gather(*handlers)

    async def init_redis_pool(self):
        self.redis_pool = await aioredis.create_pool(self.redis_url)

    async def websocket_handler(self, websocket, channel_name):
        """Handles incoming messages from the websocket connection"""
        async for message in websocket:
            logger.debug(f"Recieving: {message} | {fmt_ws(websocket)}")
            with await self.redis_pool as conn:
                await conn.execute("publish", channel_name, message)

    async def redis_handler(self, channel, websocket):
        """Handles incomming messages from the redis connection"""
        while (await channel.wait_message()):
            message = await channel.get(encoding='utf-8')
            logger.debug(f"Sending: {message} | {fmt_ws(websocket)}")
            await websocket.send(message)


def main():
    config = arg_parser().parse_args()
    logger.info(f"Starting server on {config.host}:{config.port}")
    app = App(config.redis_url)
    return websockets.serve(app.server, config.host, config.port)


def fmt_ws(websocket):
    user_agent = websocket.request_headers['User-Agent']
    origin = websocket.request_headers['Origin']
    return f'<WS {origin} {user_agent}>'


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.run_forever()
